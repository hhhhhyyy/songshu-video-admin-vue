import request from '../utils/request'

const api = {
  login: {
    login: (query) => {
      return request({
        url: '/login/login',
        method: 'post',
        data: query
      })
    },
    logout: (query) => {
      return request({
        url: '/login/logout',
        method: 'get',
        params: query
      })
    },
    isLogin: (query) => {
      return request({
        url: '/login/islogin',
        method: 'get',
        params: query
      })
    }
  },
  video: {
    list: (query) => {
      return request({
        url: '/video/getList',
        method: 'get',
        params: query,
      })
    }, delete: (query) => {
      return request({
        url: '/video/deleteVideo',
        method: 'get',
        params: query,
      })
    },
    update: (query) => {
      return request({
        url: '/video/updateVideo',
        method: 'post',
        data: query,
      })
    },
    add:(query)=>{
      return request({
        url: '/video/addVideo',
        method: 'post',
        data: query,
      })
    }
  },
  videoSubject: {
    list: (query) => {
      return request({
        url: '/subject/getList',
        method: 'get',
        params: query
      })
    },
    update: (query) => {
      return request({
        url: '/subject/saveSubject',
        method: 'post',
        data: query
      })
    },
    isSubject: (query) => {
      return request({
        url: '/subject/isSubject',
        method: 'get',
        params: query
      })
    }
  },
  textimage: {
    list: (query) => {
      return request({
        url: '/text_image/getList',
        method: 'get',
        params: query,
      })
    }, delete: (query) => {
      return request({
        url: '/text_image/delete',
        method: 'get',
        params: query,
      })
    },
    update: (query) => {
      return request({
        url: '/text_image/update',
        method: 'post',
        data: query,
      })
    }
  },
  user: {

    list: (query) => {
      return request({
        url: '/user/getList',
        method: 'get',
        params: query,
      })
    },
    invitelist: (query) => {
      return request({
        url: '/user/getInviteList',
        method: 'get',
        params: query,
      })
    },
    changelist: (query) => {
      return request({
        url: '/user/getChangeList',
        method: 'get',
        params: query,
      })
    },
    withdrawlist: (query) => {
      return request({
        url: '/user/getWithdrawList',
        method: 'get',
        params: query,
      })
    },
    withdraw: (query) => {
      return request({
        url: '/user/postwithdraw',
        method: 'post',
        params: query,
      })
    },
    listAll: (query) => {
      return request({
        url: '/user/getAllList',
        method: 'get',
        params: query,
      })
    },
    invite: (query) => {
      return request({
        url: '/user/invite',
        method: 'get',
        params: query,
      })
    },
    delete: (query) => {
      return request({
        url: '/user/deleteUser',
        method: 'get',
        params: query,
      })
    },
    update: (query) => {
      return request({
        url: '/user/updateUser',
        method: 'post',
        data: query,
      })
    },
    log: (query) => {
      return request({
        url: '/user/userLog',
        method: 'get',
        params: query,
      })
    },
    comment: (query) => {
      return request({
        url: '/comment/getList',
        method: 'get',
        params: query,
      })
    },
    commentDelete: (query) => {
      return request({
        url: '/comment/del',
        method: 'get',
        params: query,
      })
    },
    commentUpdate:(query) => {
      return request({
        url: '/comment/update',
        method: 'post',
        data: query,
      })
    },
  },
  type: {
    list: (query) => {
      return request({
        url: '/type/getList',
        method: 'get',
        params: query
      })
    },
    update: (query) => {
      return request({
        url: '/type/updateType',
        method: 'get',
        params: query
      })
    },
    add: (query) => {
      return request({
        url: '/type/addType',
        method: 'get',
        params: query
      })
    },
    delete: (query) => {
      return request({
        url: '/type/delete',
        method: 'get',
        params: query
      })
    }
  },
  config: {
    get: (query) => {
      return request({
        url: '/config/getConfig',
        method: 'get',
        params: query
      })
    },
    set: (query) => {
      return request({
        url: '/config/setConfig',
        method: 'post',
        data: query
      })
    }
  },
  admin: {
    list: (query) => {
      return request({
        url: '/manager/getList',
        method: 'get',
        data: query
      })
    },
    add: (query) => {
      return request({
        url: '/manager/addUser',
        method: 'post',
        data: query
      })
    },
    update: (query) => {
      return request({
        url: '/manager/updateUser',
        method: 'post',
        data: query
      })
    },
    delete: (query) => {
      return request({
        url: '/manager/deleteUser',
        method: 'post',
        data: query
      })
    },
    log: (query) => {
      return request({
        url: '/manager/log',
        method: 'post',
        data: query
      })
    }
  },
  update: {
    list: (query) => {
      return request({
        url: '/update/getList',
        method: 'get',
        data: query
      })
    },
    update: (query) => {
      return request({
        url: '/update/update',
        method: 'post',
        data: query
      })
    },
    delete: (query) => {
      return request({
        url: '/update/delete',
        method: 'post',
        data: query
      })
    },
    add: (query) => {
      return request({
        url: '/update/add',
        method: 'post',
        data: query
      })
    }
  },
  advert: {
    list: (query) => {
      return request({
        url: '/advert/getList',
        method: 'post',
        data: query
      })
    },
    add: (query) => {
      return request({
        url: '/advert/add',
        method: 'post',
        data: query
      })
    },
    update: (query) => {
      return request({
        url: '/advert/update',
        method: 'post',
        data: query
      })
    },
    delete: (query) => {
      return request({
        url: '/advert/delete',
        method: 'post',
        data: query
      })
    },
  },
  vipshop: {
    list: (query) => {
      return request({
        url: '/vip_shop/getList',
        method: 'post',
        data: query
      })
    },
    add: (query) => {
      return request({
        url: '/vip_shop/add',
        method: 'post',
        data: query
      })
    },
    update: (query) => {
      return request({
        url: '/vip_shop/update',
        method: 'post',
        data: query
      })
    },
    delete: (query) => {
      return request({
        url: '/vip_shop/delete',
        method: 'post',
        data: query
      })
    },
  },
  cipher: {
    list: (query) => {
      return request({
        url: '/cipher/getList',
        method: 'post',
        data: query
      })
    },
    add: (query) => {
      return request({
        url: '/cipher/add',
        method: 'post',
        data: query
      })
    },
    delete: (query) => {
      return request({
        url: '/cipher/delete',
        method: 'post',
        data: query
      })
    },
  },
  paytype: {
    list: (query) => {
      return request({
        url: '/pay_type/getList',
        method: 'post',
        data: query
      })
    },
    add: (query) => {
      return request({
        url: '/pay_type/add',
        method: 'post',
        data: query
      })
    },
    update: (query) => {
      return request({
        url: '/pay_type/update',
        method: 'post',
        data: query
      })
    },
    delete: (query) => {
      return request({
        url: '/pay_type/delete',
        method: 'post',
        data: query
      })
    },
  },
  order: {
    list: (query) => {
      return request({
        url: '/order/getList',
        method: 'post',
        data: query
      })
    },
    add: (query) => {
      return request({
        url: '/order/add',
        method: 'post',
        data: query
      })
    },
    update: (query) => {
      return request({
        url: '/order/update',
        method: 'post',
        data: query
      })
    },
    delete: (query) => {
      return request({
        url: '/order/delete',
        method: 'post',
        data: query
      })
    },
  },
  shopOrder: {
    list: (query) => {
      return request({
        url: '/ShopsOrder/getList',
        method: 'post',
        data: query
      })
    },
    add: (query) => {
      return request({
        url: '/ShopsOrder/add',
        method: 'post',
        data: query
      })
    },
    update: (query) => {
      return request({
        url: '/ShopsOrder/update',
        method: 'post',
        data: query
      })
    },
    delete: (query) => {
      return request({
        url: '/ShopsOrder/delete',
        method: 'post',
        data: query
      })
    },
  },
  shop: {
    list: (query) => {
      return request({
        url: '/shops/getList',
        method: 'post',
        data: query
      })
    },
    add: (query) => {
      return request({
        url: '/order/add',
        method: 'post',
        data: query
      })
    },
    update: (query) => {
      return request({
        url: '/order/update',
        method: 'post',
        data: query
      })
    },
    delete: (query) => {
      return request({
        url: '/order/delete',
        method: 'post',
        data: query
      })
    },
  },
  stat: {
    info: (query) => {
      return request({
        url: '/stat/getInfo',
        method: 'get',
        data: query
      })
    },

  },
  shoptype: {
    list: (query) => {
      return request({
        url: '/shops_type/getList',
        method: 'post',
        data: query
      })
    },
    add: (query) => {
      return request({
        url: '/ShopsType/add',
        method: 'post',
        data: query
      })
    },
    update: (query) => {
      return request({
        url: '/ShopsType/update',
        method: 'post',
        data: query
      })
    },
    delete: (query) => {
      return request({
        url: '/pay_type/delete',
        method: 'post',
        data: query
      })
    },
  },
}
export default api
